namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "Baiviet");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Baiviet", newName: "Posts");
        }
    }
}
