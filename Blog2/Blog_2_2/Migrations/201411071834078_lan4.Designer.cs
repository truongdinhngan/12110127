// <auto-generated />
namespace Blog_2_2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan4 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan4));
        
        string IMigrationMetadata.Id
        {
            get { return "201411071834078_lan4"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
