namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Baiviet", "ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Baiviet", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
