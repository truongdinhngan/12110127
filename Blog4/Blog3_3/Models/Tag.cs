﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3_3.Models
{
    public class Tag
    {
        [Required]
        public int ID { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Số lượng ký tự nhập vào từ 10 đến 100 ký tự", MinimumLength = 10)]
        public string Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}