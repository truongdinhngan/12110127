﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3_3.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }

        [Required]
        public String Body { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }

    }
}