namespace Blog3_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Comments", "DateUpdated");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "DateUpdated", c => c.DateTime(nullable: false));
        }
    }
}
