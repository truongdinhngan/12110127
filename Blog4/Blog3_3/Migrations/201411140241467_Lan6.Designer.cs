// <auto-generated />
namespace Blog3_3.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Lan6 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Lan6));
        
        string IMigrationMetadata.Id
        {
            get { return "201411140241467_Lan6"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
