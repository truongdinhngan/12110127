namespace Blog3_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
        }
    }
}
