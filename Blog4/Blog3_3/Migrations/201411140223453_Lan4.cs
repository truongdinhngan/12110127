namespace Blog3_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
        }
    }
}
