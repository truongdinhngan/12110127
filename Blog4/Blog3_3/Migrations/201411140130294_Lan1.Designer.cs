// <auto-generated />
namespace Blog3_3.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Lan1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Lan1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411140130294_Lan1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
