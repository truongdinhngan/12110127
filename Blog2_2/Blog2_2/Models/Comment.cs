﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}