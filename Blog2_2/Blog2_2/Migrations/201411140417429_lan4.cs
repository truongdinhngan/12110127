namespace Blog2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Baiviet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Baiviet", "Body", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Baiviet", "Body", c => c.String());
            AlterColumn("dbo.Baiviet", "Title", c => c.String());
        }
    }
}
